import sys,math

f = open(sys.argv[1] , 'r')

for line in f:
    line = line.strip()
    line = line.split(') (')

    point1 = ''.join(c for c in line[0] if c not in '()') # I get rid of the braces
    point2 = ''.join(c for c in line[1] if c not in '()')

    x1 , y1 = [int(n) for n in point1.split(', ')]
    x2 , y2 = [int(n) for n in point2.split(', ')]
    
    print int(math.sqrt((x1-x2)**2 + (y1-y2)**2))


f.close()


