import sys


f = open(sys.argv[1] , 'r')


for line in f:
    line = line.strip()
    line = line.split(' ')
    line = ''.join(line)
    sum = 0

    for i in range(0, 16, 2):
        sum = sum + int(line[i]) * 2
    for i in range(1, 17, 2):
        sum = sum + int(line[i])

    if sum % 10 == 0:
        print 'Real'
    else:
        print 'Fake'

f.close()