import sys

f = open(sys.argv[1] , 'r')

for line in f:
    line = line.strip()
    line = line.split(' ')

# long_word stores the longest word for the line
    long_word =[]
#if there's a longer word than the current long_word, then we replace it
    for word in line:
        if len(list(word)) > len(long_word):
            long_word = list(word)
    
    print ''.join(long_word)

f.close()