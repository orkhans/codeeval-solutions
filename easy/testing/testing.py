import sys

f = open (sys.argv[1], 'r')

for line in f:
    c = 0
    l1,l2 = line.split("|")
    l2 = l2.strip()
    for i in range(0,len(l1)-1):
        if l1[i] != l2[i]:
            c = c + 1
    if c == 0: print 'Done'
    elif c <= 2: print 'Low'
    elif c <= 4: print 'Medium'
    elif c <= 6: print 'High'
    else: print 'Critical'

f.close()