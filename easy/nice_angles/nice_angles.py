import sys

f = open(sys.argv[1] , 'r')

for line in f:
 
    line = line.strip()
    angle = float(line)

    degrees =int(angle)

    #now angle contains only fractional part
    angle = angle - int(angle)
    #now we need to calculate how many 1/60 parts of 1 degree are left in our fraction
    #so, we multiply the fractional part by 60 and take the integer part of the result
    minutes = int(angle * 60)

    #now I repeat the same process to get the number of seconds
    angle = angle * 60
    angle = angle - int(angle)
    seconds = int(angle * 60)

    print '%d.%02d\'%02d\"' % (degrees, minutes, seconds)

f.close()