import sys
#--------------------------------------------------------------
#This program calculates N mod M 
#--------------------------------------------------------------
f = open(sys.argv[1] , 'r')

for line in f:
    line = line.strip()
    line = line.split(',')
    # line[0] is N , line[1] is M
    sys.stdout.write( str(int(line[0]) % int(line[1])) )
    sys.stdout.write('\n')
f.close()
