import sys
# Morse code table
morse= {   '.-'	  : 'A'	,  '-...'  : 'B'  ,
		  '-.-.'  : 'C'	,  '-..'   : 'D'  ,
		  '.'	  : 'E'	,  '..-.'  : 'F'  ,
		  '--.'	  : 'G'	,  '....'  : 'H'  ,  
		  '..'	  : 'I'	,  '.---'  : 'J'  ,  
		  '-.-'	  : 'K'	,  '.-..'  : 'L'  ,   
		  '--'	  : 'M'	,  '-.'    : 'N'  ,  
		  '---'	  : 'O'	,  '.--.'  : 'P'  ,   
		  '--.-'  : 'Q'	,  '.-.'   : 'R'  ,  
		  '...'	  : 'S'	,  '-'     : 'T'  , 
		  '..-'	  : 'U'	,  '...-'  : 'V'  ,   
		  '.--'	  : 'W'	,  '-..-'  : 'X'  , 
		  '-.--'  : 'Y'	,  '--..'  : 'Z'  , 
		  '-----' : '0'	,  '.----' : '1'  ,  
		  '..---' : '2'	,  '...--' : '3'  ,  
		  '....-' : '4'	,  '.....' : '5'  ,  
		  '-....' : '6'	,  '--...' : '7'  ,  
		  '---..' : '8'	,  '----.' : '9'   }

f = open(sys.argv[1] , 'r')

for line in f:
    line = line.strip()
    #I split the line into multiple words using '  ' delimiter
    line = line.split('  ')
    #I split every word so I can go through every single Morse sequence, representing one character
    for word in line:
        word = word.split(' ')
        for ch in word:
            sys.stdout.write(morse[ch])
        sys.stdout.write(' ')
    print ''



f.close()
