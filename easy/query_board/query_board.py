import sys

def SetCol(matrix, col, val):
    for i in range(0,256):
        matrix[i][col-1] = val

def SetRow(matrix, row, val):
    for i in range(0,256):
        matrix[row-1][i] = val

def QueryRow(matrix, row):
    sum = 0
    for i in range(0,256):
        sum += matrix[row-1][i]
    return sum

def QueryCol(matrix, col):
    sum = 0
    for i in range(0,256):
        sum += matrix[i][col-1]
    return sum

f = open(sys.argv[1] , 'r')

board=[]

# I initialize the matrix
for i in range(0,256):
    board.append([])
    for j in range(0,256):
        board[i].append(0)


for line in f:
    line = line.strip()
    line = line.split(' ')
    # the command is read and the corresponding function is triggered
    if line[0] == 'SetCol':
        SetCol(board, int(line[1]), int(line[2]))
    elif line[0] == 'SetRow':
        SetRow(board, int(line[1]), int(line[2]))
    elif line[0] == 'QueryRow':
        print QueryRow(board, int(line[1]))
    else:
        print QueryCol(board, int(line[1]))
    





f.close()