import sys

f = open(sys.argv[1] , 'r')

for word in f:
    
    word = word.strip()
    # The default period is the length of the string. If we find any shorter period we will overwrite the period value
    period = len(word)
    # We start with the first character and see if it is a period of our string.
    #Then we add the second character and so on. It makes sense only to check substrings which are shorter than the half of the whole string
    for i in range(0, len(word)/2 - 1):
        #here we create a new slice of a string
        w = word[:i+1]
        #we multiply our possible period and see if it matches the whole string.
        #if it does we set the period value and quit the loop, because we only need the shortest period.
        if ( w * (len(word)/(i+1))) == word:
            period = i+1
            break
    #we print out period which has either default value or a value set in our loop 
    print period



f.close()