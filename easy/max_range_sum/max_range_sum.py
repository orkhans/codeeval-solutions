
import sys

f = open(sys.argv[1], 'r')

# this function returns the sum for the specific range of days
def gain(index, days, history): 
    sum = 0
    for i in range (index,index+days):
        sum += int(history[i])
    return sum

for line in f:

    line = line.strip()
    line = line.split(';')
    days = int(line[0]) # the range of days
    history = line[1].split(' ') # history contains the values of gains and losses for each day

    max = 0


# if function "gain" returns value greater than
# max, then max gets overwritten

    for index in range(0,len(history)-days+1):
        if gain(index,days,history) > max: max = gain(index,days,history) 

    print max

f.close()