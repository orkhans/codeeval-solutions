# the idea is that the most frequent character should get the highest beauty
# the second most frequent character should get the second highest beauty and so on

import sys

f = open(sys.argv[1], 'r')

for line in f:
    line = line.strip()
    line = list(line)
    array = [0] * 26 # The array will store the number of occurences for each character from A to Z
    for char in line:
        ch = char.upper()
        if ord(ch)>=65 and ord(ch)<=90: # we ignore non alpha characters
            array[ord(ch)-65] += 1
    sum = 0
    #we sort the array and start with most frequent character which gets the higher beauty
    array.sort()          
    
    for i in range(25,-1,-1):
        if array[i]==0: break
        sum += (i+1) * array[i]

    print sum

f.close()
