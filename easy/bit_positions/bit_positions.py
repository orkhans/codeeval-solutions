import sys
#--------------------------------------------------------------
#This program checks if bits in specific positions are the same 
#--------------------------------------------------------------
f = open(sys.argv[1] , 'r')

for line in f:
    line = line.strip()
    line = line.split(',')
    # binary contains bits in binary format without leading '0b' 
    binary = bin(int(line[0]))[2:]
    #print binary
    #print binary[int(line[1])-1]
    #print binary[int(line[2])-1]

    if binary[-int(line[1])] == binary[-int(line[2])]:
        sys.stdout.write('True\n'.lower())
    else:
        sys.stdout.write('False\n'.lower())
    
f.close()
