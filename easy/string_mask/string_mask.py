import sys

f = open(sys.argv[1] , 'r')

for line in f:
    line = line.strip()
    word , mask = line.split(' ')
    word = list(word)
    for i in range(0,len(word)):
        if mask[i]=='1': word[i] = word[i].upper()

    print ''.join(word)

f.close()
