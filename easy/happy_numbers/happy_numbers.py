import sys


f = open(sys.argv[1] , 'r')


for line in f:
 
    numbers = set()

    number = int(line.strip())
    summ = 0
    while (number != 1):
        number = sum ( int(n)**2 for n in str(number))
        # if the result is in our set then there's a loop and we need to break
        if number in numbers:
            break
        else:
            numbers.add(number)
    if number == 1:
        print 1
    else:
        print 0




f.close()