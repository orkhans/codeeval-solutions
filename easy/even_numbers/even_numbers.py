import sys

f = open(sys.argv[1] , 'r')

for line in f:
    line = line.strip()

    n = int(line)
# print 1 if number is even, otherwise print 0
    if (n % 2) == 0:
        print 1
    else:
        print 0

f.close()