import sys
from datetime import datetime
# The problem can be interpreted as a problem with a classical Fibonacci algorithm solution.
#There are only two ways we can get to any step N in one action(we either climb 1 or 2 steps), 
#so we need to sum up all possible ways of climbing to N-2 and N-1. This is Fibonacci sequence.

time = datetime.now()

f = open(sys.argv[1] , 'r')

m = {0: 0, 1: 1}

def fib(n):
    
    if n not in m:
        m[n] = fib(n-1) + fib(n-2)
    return m[n]


for line in f:
    line = line.strip()
    if len(line) == 0:
        continue
    n = int(line)
    print fib(n+1)

f.close()

print datetime.now() - time
