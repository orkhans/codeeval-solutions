import sys

Dict_hundreds = {'1' : 'One' , '2' : "Two" , '3' : 'Three' , '4' : 'Four' , '5' : 'Five',
                  '6' : 'Six' , '7' : 'Seven' , '8' : 'Eight' , '9' : 'Nine'  }

Dict_tens = {'2' : "Twenty" , '3' : 'Thirty' , '4' : 'Forty' , '5' : 'Fifty',
            '6' : 'Sixty' , '7' : 'Seventy' , '8' : 'Eighty' , '9' : 'Ninety'  }

Dict_tens_ones = { '00' : '' , '01' : 'One' , '02' : "Two" , '03' : 'Three' , '04' : 'Four' , '05' : 'Five' ,
        '06' : 'Six' , '07' : 'Seven' , '08' : 'Eight' , '09' : 'Nine' , '10' : 'Ten', 
        '11' : 'Eleven' , '12' : 'Twelve' , '13' : 'Thirteen' , '14' : 'Fourteen' , '15' : 'Fifteen',
        '16' : 'Sixteen' , '17' : 'Seventeen' , '18' : 'Eighteen' ,'19' : 'Nineteen',
        '20' : 'Twenty' , '30' : 'Thirty' , '40' : 'Forty' , '50' : 'Fifty' , '60' : 'Sixty',
        '70' : 'Seventy' , '80' : 'Eighty' , '90' : 'Ninety'}

def getWord(number, startPos, scale = ''):
    word = ''
    #if the digit at startPos is 0 then there are no hundreds
    if number[startPos] != '0':
        word = Dict_hundreds[number[startPos]] + 'Hundred'
    # Dict_tens_ones contains combinations of tens and ones which should be parsed together like Eleven or Nineteen.
    tens_ones = ''.join(number[startPos+1 : startPos+3 ])
    if tens_ones in Dict_tens_ones:
        word += Dict_tens_ones[tens_ones]
    else:
        tens = number[startPos+1]
        word += Dict_tens[tens]
        ones = number[startPos+2]
        word += Dict_hundreds[ones]
    if word != '' : 
        word += scale

    return word



f = open(sys.argv[1] , 'r')


for line in f:
 
    line = line.strip()
    line = list(line)
    #i add '0' so the list contains exactly 9 digits  
    line = ['0'] * (9 - len(line)) + line

    ending = 'Dollars'

    print getWord(line , 0 , 'Million') + getWord(line , 3 , 'Thousand') + getWord(line , 6) + ending


f.close()